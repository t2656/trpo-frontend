import {Component, Input, OnInit} from '@angular/core';
import {CategoryCard} from "../../../../shared/models/CategoryCard";

@Component({
  selector: 'app-category-card',
  templateUrl: './category-card.component.html',
  styleUrls: ['./category-card.component.less']
})
export class CategoryCardComponent implements OnInit {

  @Input()
  cardCategory!: CategoryCard;

  constructor() { }

  ngOnInit(): void {
  }

}
