import {Component, Input, OnInit} from '@angular/core';
import {CartProductItem} from "../../../../shared/models/CartProductItem";
import {ShoppingCardService} from "../../../../shared/services/shopping-card/shopping-card.service";

@Component({
  selector: 'app-shopping-card',
  templateUrl: './shopping-card.component.html',
  styleUrls: ['./shopping-card.component.less']
})
export class ShoppingCardComponent implements OnInit {

  @Input()
  product!: CartProductItem;

  constructor(private shoppingCartService: ShoppingCardService) {
    //Todo пересмотреть подход в будущем
    shoppingCartService.eventUpdate.subscribe(status => {
      if (this.product.id != undefined) {
        let updatedProduct = shoppingCartService.getProductFromCart(this.product.id);
        if (updatedProduct != undefined) {
          this.product = updatedProduct;
        }
      }
    });
  }

  ngOnInit(): void {
  }

  public addProduct(): void {
    console.log('id_product is ' + this.product.id);
    this.shoppingCartService.addProductToCard(
      this.product.id,
      this.product.name,
      this.product.link,
      this.product.price
    );

  }

  public deleteProduct(): void {
    console.log('id_product is ' + this.product.id);
    this.shoppingCartService.deleteProductToCard(this.product.id);
  }

}
