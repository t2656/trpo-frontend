import { Component, OnInit, Input } from '@angular/core';
import {ProductCard} from "../../../../shared/models/ProductCard";
import {ShoppingCardService} from "../../../../shared/services/shopping-card/shopping-card.service";

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.less']
})
export class ProductCardComponent implements OnInit {
  //https://stackoverflow.com/questions/49699067/property-has-no-initializer-and-is-not-definitely-assigned-in-the-construc
  @Input() productCard!: ProductCard;

  constructor(private shoppingCartService: ShoppingCardService) {
  }

  ngOnInit(): void {
  }
  //https://uprostim.com/wp-content/uploads/2021/05/image032-6.jpg

  public addProductToCart() {
    console.log("Add");
    this.shoppingCartService.addProductToCard(this.productCard.id+'', this.productCard.name, this.productCard.photos[0], this.productCard.price); //Fixme please!!! убери костыль
  }
}
