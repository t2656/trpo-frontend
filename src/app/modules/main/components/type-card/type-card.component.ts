import {Component, Input, OnInit} from '@angular/core';
import {TypeCard} from "../../../../shared/models/TypeCard";

@Component({
  selector: 'app-type-card',
  templateUrl: './type-card.component.html',
  styleUrls: ['./type-card.component.less']
})
export class TypeCardComponent implements OnInit {

  @Input()
  cardType!: TypeCard;

  constructor() { }

  ngOnInit(): void {
  }

}
