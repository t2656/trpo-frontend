import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainRoutingModules} from "./main-routing.modules";
import {HomeComponent} from './pages/home/home.component';
import {ProductComponent} from './pages/product/product.component';
import {CoreModule} from "../../core/core.module";
import {CatalogComponent} from './pages/catalog/catalog.component';
import {ProductCardComponent} from './components/product-card/product-card.component';
import {TypeCardComponent} from './components/type-card/type-card.component';
import {MatButtonModule} from "@angular/material/button";
import {MatTabsModule} from '@angular/material/tabs';
import {ShoppingCartComponent} from './pages/shopping-cart/shopping-cart.component';
import {MatTableModule} from '@angular/material/table';
import {CatalogCategoriesComponent} from './pages/catalog-categories/catalog-categories.component';
import {CatalogProductComponent} from './pages/catalog-product/catalog-product.component';
import {CategoryCardComponent} from './components/category-card/category-card.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ShoppingCardComponent} from "./components/shopping-card/shopping-card.component";
import {NotFoundComponent} from './pages/not-found/not-found.component';
import {CreateOrderComponent} from './pages/create-order/create-order.component';
import {ResultSearchComponent} from './pages/result-search/result-search.component';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    HomeComponent,
    ProductComponent,
    CatalogComponent,
    ProductCardComponent,
    TypeCardComponent,
    ShoppingCartComponent,
    ShoppingCardComponent,
    CatalogCategoriesComponent,
    CatalogProductComponent,
    CategoryCardComponent,
    NotFoundComponent,
    CreateOrderComponent,
    ResultSearchComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MainRoutingModules,
    CoreModule,
    MatButtonModule,
    MatTabsModule,
    MatTableModule,
    NgbModule
  ]
})
export class MainModule {
}
