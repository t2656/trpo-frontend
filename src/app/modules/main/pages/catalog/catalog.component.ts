import {Component, OnInit} from '@angular/core';
import {TypeCard} from "../../../../shared/models/TypeCard";
import {HttpService} from "../../../../shared/services/http/http.service";

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.less']
})
export class CatalogComponent implements OnInit {

  typeCards!: TypeCard[];

  constructor(private httpService: HttpService) {
    httpService.getAllTypes().subscribe(
      (data: any) => this.typeCards = data);
  }

  ngOnInit(): void {
  }

}
