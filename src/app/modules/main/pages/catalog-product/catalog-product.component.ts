import {Component, OnInit} from '@angular/core';
import {ProductCard} from "../../../../shared/models/ProductCard";
import {Subscription} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {HttpService} from "../../../../shared/services/http/http.service";

@Component({
  selector: 'app-catalog-product',
  templateUrl: './catalog-product.component.html',
  styleUrls: ['./catalog-product.component.less']
})
export class CatalogProductComponent implements OnInit {

  productsCard!: ProductCard[];
  nameCategory!: string;
  id_category: any;
  private routeSubscription: Subscription;

  constructor(private route: ActivatedRoute, private httpService: HttpService) {
    this.routeSubscription = route.params.subscribe(params => this.id_category = params['id_category']);
    httpService.getProductsByCategoryId(this.id_category).subscribe(
      (data: any) => {
        this.productsCard = data.products;
        this.nameCategory = data.name;
      });
  }

  ngOnInit(): void {
  }

}
