import { Component, OnInit } from '@angular/core';
import {HttpService} from "../../../../shared/services/http/http.service";
import {TypeCard} from "../../../../shared/models/TypeCard";

const lengthOfCatalog = 9;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {

  cardTypes!: TypeCard[]; //https://uprostim.com/wp-content/uploads/2021/05/image034-5.jpg

  images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/1920/1080`);
  constructor(private httpService: HttpService) {
    //Fixme
    httpService.getAllTypes().subscribe(
      (data:any) => {
        let carts: TypeCard[] = data;
        console.log(carts.length)
        if(carts.length > lengthOfCatalog) {
          this.cardTypes = carts.slice(0, lengthOfCatalog);;
        }
        else {
          this.cardTypes = carts;
        }
      });
  }

  ngOnInit(): void {
  }

}
