import {Component, OnInit} from '@angular/core';
import {CategoryCard} from "../../../../shared/models/CategoryCard";
import {HttpService} from "../../../../shared/services/http/http.service";
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-catalog-categories',
  templateUrl: './catalog-categories.component.html',
  styleUrls: ['./catalog-categories.component.less']
})
export class CatalogCategoriesComponent implements OnInit {

  categoryCards!: CategoryCard[];
  type_id: any;
  nameOfType!: string;
  private routeSubscription: Subscription;

  constructor(private route: ActivatedRoute, private httpService: HttpService) {
    this.routeSubscription = route.params.subscribe(params => this.type_id = params['id_type']);
    httpService.getCategoriesByTypeId(this.type_id).subscribe(
      (data: any) => {
        this.categoryCards = data.categories;
        this.nameOfType = data.name;
      });
  }

  ngOnInit(): void {
  }

}
