import {Component, OnInit} from '@angular/core';
import {ShoppingCardService} from "../../../../shared/services/shopping-card/shopping-card.service";
import {CartProductItem} from "../../../../shared/models/CartProductItem";
import {AuthorizationService} from "../../../../shared/services/authorization/authorization.service";
import {HttpService} from "../../../../shared/services/http/http.service";
import {Order} from "../../../../shared/models/Order";

const DELIVERY_STORE = "store";
const DELIVERY_HOME = "home";

@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.less']
})
export class CreateOrderComponent implements OnInit {

  isCollapsed = true;
  cards: CartProductItem[];
  totalSum: number;
  order!: Order;
  resultCreation: string | null;


  constructor(private shoppingService: ShoppingCardService, private authService: AuthorizationService, private httpService: HttpService) {
    this.cards = shoppingService.getItems();
    if (this.cards.length == 0) {
      //todo если заходят на рут с пустой корзиной
      //Fixme переделать нормально с отображением ошибки
      window.location.href = '/';
    }
    this.resultCreation = null;
    this.totalSum = shoppingService.createShoppingCartInfo().sumFinish;
    let shoppingCart = shoppingService.getItems();
    let shoppingCartInfo = shoppingService.createShoppingCartInfo();
    //Todo навешать проверок
    this.order = {
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      comment: '',
      delivery: DELIVERY_STORE,
      homeAddress: '',
      payment: 'credit_card',
      shoppingCart: shoppingCart,
      shoppingCartInfo: shoppingCartInfo
    };
    let user = authService.getUser();
    if (user != undefined) {
      this.order.firstName = user.firstName;
      this.order.lastName = user.lastName;
      this.order.email = user.email;
      this.order.phone = user.phone;
      this.order.userId = user.id;
    }

    shoppingService.eventUpdate.subscribe(event => {
      this.cards = shoppingService.getItems();
      this.totalSum = shoppingService.createShoppingCartInfo().sumFinish;
    });
  }

  ngOnInit(): void {
  }

  public collapseSet(checked: boolean) {
    this.isCollapsed = checked;
  }

  public createOrder() {
    if (this.validateOrder(this.order)) {
      this.httpService.createOrder(this.order).subscribe(
        (data: any) => {
          this.resultCreation = "Order is success created. Number of order is " + data.numberOrder;
          this.shoppingService.clearCart();
        },
        (error: any) => {
          this.resultCreation = "Order is not created. Error is " + error.numberOrder;
        }
      )
    }
  }

  private validateOrder(order: Order): boolean {
    let result: boolean = true;
    console.log(order);
    if (order.firstName == null || order.firstName.length < 2) {
      result = false;
      alert("Order is not created. Field First name is not valid!")
    }
    if (order.lastName == null || order.lastName.length < 2) {
      result = false;
      alert("Order is not created. Field Last name is not valid!")
    }
    if (order.shoppingCart.length == 0) {
      result = false;
      alert("Order is not created. Shopping cart is empty!")
    }
    //Todo вынести проверку в отдельный сервис
    if (order.phone == null || order.phone.length != 11) {
      result = false;
      alert("Order is not created. Field Phone is not valid!");
    }
    //Todo вынести проверку в отдельный сервис
    if (order.email == null || order.email.length < 5) {
      result = false;
      alert("Order is not created. Field Email is not valid!");
    }
    if (order.payment == null || order.payment.length < 2) {
      result = false;
      alert("Order is not created. Field Payment name is not valid!");
    }
    if (order.delivery == null || order.delivery.length < 2) {
      result = false;
      alert("Order is not created. Field Delivery name is not valid!");
    }
    if (order.delivery != null && order.delivery == DELIVERY_HOME && (order.homeAddress == null || order.homeAddress.length < 1)) {
      result = false;
      alert("Order is not created. Field Address is not valid!");
    }
    return result;
  }
}
