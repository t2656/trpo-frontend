import { Component, OnInit, Input } from '@angular/core';
import {Product} from "../../../../shared/models/Product";
import {HttpService} from "../../../../shared/services/http/http.service";
import {Subscription} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {ShoppingCardService} from "../../../../shared/services/shopping-card/shopping-card.service";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.less']
})
export class ProductComponent implements OnInit {

  product!: Product;
  id: any;
  private routeSubscription: Subscription;

  constructor(private route: ActivatedRoute, private httpService: HttpService, private shoppingCardService: ShoppingCardService) {
    this.routeSubscription = route.params.subscribe(params => this.id = params['id_product']);
    httpService.getProduct(this.id).subscribe(
      (data:any) =>{
        this.product = data;
        console.log(this.product);
      });

  }

  ngOnInit(): void {
  }

  public addToCart(): void {
    this.shoppingCardService.addProductToCard(this.product.id, this.product.name, this.product.photos[0], this.product.price);
  }

}
