import {Component, OnInit} from '@angular/core';
import {ShoppingCardService} from "../../../../shared/services/shopping-card/shopping-card.service";
import {CartProductItem} from "../../../../shared/models/CartProductItem";
import {ShoppingCartInfo} from "../../../../shared/models/ShoppingCartInfo";
import {AuthorizationService} from "../../../../shared/services/authorization/authorization.service";

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.less']
})
export class ShoppingCartComponent implements OnInit {

  dataSource: CartProductItem[] = [];
  orderSummary: ShoppingCartInfo;
  canCreateOrder: boolean = false;

  constructor(private shoppingCartService: ShoppingCardService, private authService: AuthorizationService) {

    this.dataSource = shoppingCartService.getItems();
    this.orderSummary = shoppingCartService.createShoppingCartInfo();
    this.canCreateOrder = authService.getUser() != undefined;

    //Fixme
    this.shoppingCartService.eventUpdate.subscribe((status) => {
      this.dataSource = shoppingCartService.getItems();
      this.orderSummary = shoppingCartService.createShoppingCartInfo();
    });
    this.authService.eventUpdateAuth.subscribe(event => {
      this.canCreateOrder = authService.getUser() != undefined;
    })
  }

  ngOnInit(): void {

  }

  public clearCart(): void {
    this.shoppingCartService.clearCart();
  }

}
