import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from "./pages/home/home.component";
import {ProductComponent} from "./pages/product/product.component";
import {CatalogComponent} from "./pages/catalog/catalog.component";
import {ShoppingCartComponent} from "./pages/shopping-cart/shopping-cart.component";
import {CatalogCategoriesComponent} from "./pages/catalog-categories/catalog-categories.component";
import {CatalogProductComponent} from "./pages/catalog-product/catalog-product.component";
import {NotFoundComponent} from "./pages/not-found/not-found.component";
import {CreateOrderComponent} from "./pages/create-order/create-order.component";


const routes: Routes = [

  //Todo подумать над дочерними маршрутами для каталога
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'catalog',
    component: CatalogComponent
  },
  {
    path: 'catalog/:id_type/categories',
    component: CatalogCategoriesComponent
  },
  {
    path: 'products/:id_category',
    component: CatalogProductComponent
  },
  {
    path: 'product/:id_product',
    component: ProductComponent
  },
  {
    path: 'shopping-cart',
    component: ShoppingCartComponent
  },
  {
    path: 'create-order',
    component: CreateOrderComponent
  }
  /* Todo перенос реализации в следующую версию
  {
    path: 'search',
    component: ResultSearchComponent
  },*/
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModules {
}
