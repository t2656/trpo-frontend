import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UserProfileComponent} from "./pages/user-profile/user-profile.component";
import {OrderHistoryComponent} from "./pages/order-history/order-history.component";
import {EditProfileComponent} from "./pages/edit-profile/edit-profile.component";
import {OrderComponent} from "./pages/order/order.component";


const routes: Routes = [


  {
    path: 'profile',
    component: UserProfileComponent
  },
  {
    path: 'profile/edit',
    component: EditProfileComponent
  },
  {
    path: 'orders',
    component: OrderHistoryComponent
  },
  {
    path: 'orders/:id_order',
    component: OrderComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyerRoutingModules {
}
