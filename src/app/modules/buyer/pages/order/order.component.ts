import {Component, OnInit} from '@angular/core';
import {Order} from "../../../../shared/models/Order";
import {HttpService} from "../../../../shared/services/http/http.service";
import {Subscription} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {ShoppingCardService} from "../../../../shared/services/shopping-card/shopping-card.service";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.less']
})
export class OrderComponent implements OnInit {

  //Todo в будущем перетащить компонент в main и добавить страницук для поиска заказа по номеру для незарегистрированных пользователей

  isCollapsed = true;
  order!: Order;
  id: any;
  private routeSubscription: Subscription;

  constructor(private route: ActivatedRoute, private httpService: HttpService, private shoppingCardService: ShoppingCardService) {
    this.routeSubscription = route.params.subscribe(params => this.id = params['id_order']);
    httpService.getOrder(this.id).subscribe(
      (data: any) => {
        this.order = data;
        this.isCollapsed = this.order.delivery == 'store';
        console.log(this.order);
      });

  }

  ngOnInit(): void {
  }

  public collapseSet(checked: boolean) {
    this.isCollapsed = checked;
  }

  public declineOrder() {
    if (this.order?.isCanDecline) {
      console.log("Decline order");
      this.httpService.declineOrder(this.order).subscribe(
        (data: any) => {
          console.log('success decline');
          console.log(data.status);
          window.location.href = '/user/orders';
        },
        (error: any) => {
          console.log(error);
        });
    }
  }

  public approveOrder() {
    if (this.order?.isCanReceive) {
      console.log("Approve order");
      this.httpService.approveOrder(this.order)
        .subscribe(
          (data: any) => {
            console.log('success approve');
            console.log(data.status);
            window.location.href = '/user/orders';
          },
          (error: any) => {
            console.log(error);
          });
    }
  }

  public payOrder() {
    if (this.order?.isCanPay) {
      console.log("Pay order");
      this.httpService.payOrder(this.order).subscribe(
        (data: any) => {
          console.log('success pay');
          console.log(data.status);
          window.location.href = '/user/orders';
        },
        (error: any) => {
          console.log(error);
        });
    }
  }

}
