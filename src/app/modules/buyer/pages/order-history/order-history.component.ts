import {Component, OnInit} from '@angular/core';
import {NgModule} from '@angular/core';
import {HttpService} from "../../../../shared/services/http/http.service";
import {User} from "../../../../shared/models/User";
import {AuthorizationService} from "../../../../shared/services/authorization/authorization.service";

interface Order {
  num?: number;
  id: string;
  dateCreation: string;
  status: string;
  sum: number;
  volute: string;
  isPay: boolean;
}

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.less']
})
export class OrderHistoryComponent implements OnInit {

  page = 1;
  pageSize = 4;
  collectionSize = 0;
  orders!: Order[];
  prepareOrders!: Order[];

  constructor(private httpService: HttpService, private authService: AuthorizationService) {
    let user = this.authService.getUser();
    if (user != undefined) {
      this.httpService.getAllUserOrders(user.id).subscribe((data: any) => {
        this.prepareOrders = data;
        this.orders = this.prepareOrders.map((order, i) => ({num: i + 1, ...order}))
          .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
        console.log(this.orders);
        this.collectionSize = this.prepareOrders.length;

      });
    }
  }

  refreshCountries() {
    this.orders = this.prepareOrders
      .map((order, i) => ({num: i + 1, ...order}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  ngOnInit(): void {
  }

}
