import {Component, OnInit} from '@angular/core';
import {AuthorizationService} from "../../../../shared/services/authorization/authorization.service";
import {User} from "../../../../shared/models/User";
import {HttpService} from "../../../../shared/services/http/http.service";


@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.less']
})
export class EditProfileComponent implements OnInit {

  user!: User;
  cloneUser!: User;
  currentPassword: string = "";
  newPassword: string = "";
  confirmPassword: string = "";
  changePasswordTest?: any;

  constructor(private authService: AuthorizationService, private httpService: HttpService) {
    let user = authService.getUser();
    if (user != undefined) {
      this.user = user;
      this.cloneUser = {
        id: this.user.id, firstName: this.user.firstName, lastName: this.user.lastName,
        email: this.user.email, phone: this.user.phone, photo: this.user.photo
      };
    }
    this.changePasswordTest = {
      currentPassword: this.currentPassword,
      newPassword: this.newPassword,
      confirmPassword: this.confirmPassword
    };

  }

  ngOnInit(): void {
  }

  public saveChanges() {
    if (!this.validateName(this.user.firstName)) {
      return alert("Changes is not saved. First name is invalid!")
    }
    if (!this.validateName(this.user.lastName)) {
      return alert("Changes is not saved. Last name is invalid!")
    }
    if (!this.validateEmail(this.user.email)) {
      return alert("Changes is not saved. Email is invalid!")
    }
    if (!this.validatePhoneNum(this.user.phone)) {
      return alert("Changes is not saved. Phone is invalid!")
    }
    if (!this.equalUser(this.user, this.cloneUser)) {
      this.httpService.updateUserProfile(this.user).subscribe(
        data => {
          console.log(data);
          alert("Changes success saved.")
        },
        error => {
          console.log(error);
          alert("Changes not saved.")
        }
      );
    } else {
      alert("Not changes.")
    }
  }

  //Todo перенести валидации в отдельный сервис валидации в ядре
  private validateName(name: string): boolean {
    let result: boolean = false;
    if (name != null && name.length > 1) {
      result = true;
    }
    return result;
  }

  private validatePhoneNum(phone: string): boolean {
    let result: boolean = false;
    if (phone != null && phone.length == 11) {
      result = true;
    }
    return result;
  }

  private validateEmail(email: string): boolean {
    let result: boolean = false;
    let regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    if (email != null && email.length > 10 && regexp.test(email)) {
      result = true;
    }
    return result;
  }


  private equalUser(user1: User, user2: User): boolean {
    return user1.firstName == user2.firstName &&
      user1.lastName == user2.lastName &&
      user1.email == user2.email &&
      user1.phone == user2.phone &&
      user1.photo == user2.photo
  }

  public changePassword() {
    if (!this.validatePassword(this.currentPassword)) {
      return alert('Current password in not valid')
    }
    if (!this.validatePassword(this.newPassword)) {
      return alert('New password in not valid')
    }
    if (!this.validatePassword(this.confirmPassword)) {
      return alert('Confirm password in not valid')
    }
    if (this.newPassword != this.confirmPassword) {
      return alert('New password is not equal confirm password')
    }
    if (this.newPassword == this.currentPassword) {
      return alert('New password does not equal current password')
    }


    this.httpService.updateUserPassword(this.user.id, this.currentPassword, this.newPassword).subscribe(
      data => {
        console.log(data);
        alert('Password success changed');
      },
      error => {
        console.log(error);
        alert('Password is not changed! ' + error);
      }
    );
  }

  private validatePassword(password: string): boolean {
    return password != null && password.length >= 8
  }

  public changePhoto() {

  }

}
