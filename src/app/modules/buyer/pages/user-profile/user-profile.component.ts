import {Component, OnInit} from '@angular/core';
import {UserProfile} from "../../../../shared/models/UserProfile";
import {AuthorizationService} from "../../../../shared/services/authorization/authorization.service";
import {HttpService} from "../../../../shared/services/http/http.service";

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.less'],
})
export class UserProfileComponent implements OnInit {

  public userProfile!: UserProfile;

  constructor(private authService: AuthorizationService, private httpService: HttpService) {
    let user = authService.getUser();
    if (user != undefined) {
      this.httpService.getUserProfile(user.id).subscribe((data: any) => this.userProfile = data);
    }
  }

  ngOnInit(): void {
  }

  public logout() {
    this.authService.logout();
  }
}
