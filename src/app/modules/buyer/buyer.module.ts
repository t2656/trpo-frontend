import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import { OrderHistoryComponent } from './pages/order-history/order-history.component';
import {CoreModule} from "../../core/core.module";
import {BuyerRoutingModules} from "./buyer-routing-modules.module";
import { EditProfileComponent } from './pages/edit-profile/edit-profile.component';
import { OrderComponent } from './pages/order/order.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';




@NgModule({
  declarations: [
    UserProfileComponent,
    OrderHistoryComponent,
    EditProfileComponent,
    OrderComponent
  ],
  imports: [
    CommonModule,
    CoreModule,
    BuyerRoutingModules,
    CommonModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule
  ]
})
export class BuyerModule { }
