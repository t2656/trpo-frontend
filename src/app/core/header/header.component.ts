import {Component, OnInit} from '@angular/core';
import {ShoppingCardService} from "../../shared/services/shopping-card/shopping-card.service";
import {User} from "../../shared/models/User";
import {AuthorizationService} from "../../shared/services/authorization/authorization.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

  user: User | undefined; //Todo реализовать в задаче с авторизацией
  shoppingCartSize: number;

  constructor(private shoppingCartService: ShoppingCardService, private authService: AuthorizationService) {
    this.user = this.authService.getUser();
    this.shoppingCartSize = this.shoppingCartService.getSizeCard();// Fixme
    this.shoppingCartService.eventUpdate.subscribe((status) => {
      this.shoppingCartSize = this.shoppingCartService.getSizeCard();
    });
    this.authService.eventUpdateAuth.subscribe(status => {
      this.user = this.authService.getUser();
    });
  }

  ngOnInit(): void {
  }

  public signIn() {
    this.authService.signIn();
  }

  public logout() {
    this.authService.logout();
  }

}
