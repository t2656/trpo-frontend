export class Price {
  currentPrice: number;
  oldPrice: number;
  volute_symbol: string;

  constructor(currentPrice: number, oldPrice: number, valute_symbol: string) {
    this.currentPrice = currentPrice;
    this.oldPrice = oldPrice;
    this.volute_symbol = valute_symbol;
  }
}
