export class CategoryCard {
  id: string;
  name: string;
  description: string;
  photo: string;

  constructor(id: string, name: string, description: string, photo: string) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.photo = photo;
  }
}
