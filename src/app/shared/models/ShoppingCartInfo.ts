export class ShoppingCartInfo {
  sizeProducts: number;
  sumOfProducts: number;
  sumDiscount: number;
  sumFinish: number;
  volute_symbol: string;


  constructor(sizeProducts: number, sumOfProducts: number, sumDiscount: number, sumFinish: number, volute_symbol: string) {
    this.sizeProducts = sizeProducts;
    this.sumOfProducts = sumOfProducts;
    this.sumDiscount = sumDiscount;
    this.sumFinish = sumFinish;
    this.volute_symbol = volute_symbol;
  }

}
