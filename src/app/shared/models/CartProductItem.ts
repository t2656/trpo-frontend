import {Price} from "./Price";

export class CartProductItem {
  id: string;
  name: string;
  link: string;
  price: Price;
  count: number;

  constructor(id: string, name: string, link: string, price: Price, count: number) {
    this.id = id;
    this.name = name;
    this.link = link;
    this.price = price;
    this.count = count;
  }
}
