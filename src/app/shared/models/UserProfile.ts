export class UserProfile {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  photo: string;
  countOrders: number;


  constructor(id: number, firstName: string, lastName: string, email: string, phone: string, photo: string, countOrders: number) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.phone = phone;
    this.photo = photo;
    this.countOrders = countOrders;
  }
}
