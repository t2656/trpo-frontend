import {CartProductItem} from "./CartProductItem";
import {ShoppingCartInfo} from "./ShoppingCartInfo";

export interface Order {
  id?: string;
  userId?: string;
  status?: string;
  dateCreation?: string;
  dateLastChange?: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  comment: string;
  delivery: string;
  homeAddress?: string;
  payment: string;
  shoppingCart: CartProductItem[];
  shoppingCartInfo: ShoppingCartInfo;
  isCanReceive?: boolean;
  isCanDecline?: boolean;
  isCanPay?:boolean;
}
