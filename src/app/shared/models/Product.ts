import {Price} from "./Price";

export class Product {
  id: string;
  name: string;
  price: Price;
  photos: string[];
  description: string;
  reviews: string[];
  specifications: string;

  constructor(id: string, name: string, price: Price, photos: string[], description: string, reviews: string[], specifications: string) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.photos = photos;
    this.description = description;
    this.reviews = reviews;
    this.specifications = specifications;
  }
}
