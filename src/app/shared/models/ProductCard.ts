import {Price} from "./Price";

export interface ProductCard {
  id: number;
  name: string;
  price: Price;
  photos: string[];
}
