import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from "../../models/User";
import {Order} from "../../models/Order";

//Todo вынести в файл настроек
const hostServer = "http://localhost";
const portServer = "8080";
const urlServer = hostServer + ':' + portServer;
const routes = new Map<string, string>([
  ['getProductById', urlServer + '/products?id='],
  ['getAllTypes', urlServer + '/types'],
  ['getCategoriesByTypeId', urlServer + '/categories?type_id='],
  ['getProductsByCategoryId', urlServer + '/products?category_id='],
  ['getAllUserOrders', urlServer + '/orders?user_id='],
  ['updateUserProfile', urlServer + '/user/profile'],
  ['getUserProfile', urlServer + '/user/profile?user_id='],
  ['updateUserPassword', urlServer + '/user/update-pass'],
  ['createOrder', urlServer + '/orders/create-order'],
  ['getOrderById', urlServer + '/orders/order?order_id='],
  ['approveOrder', urlServer + '/orders/approve-order'],
  ['payOrder', urlServer + '/orders/pay-order'],
  ['declineOrder', urlServer + '/orders/decline-order'],
]);

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) {
  }

  //Todo вынести в отдельные сервисы
  getProduct(id: string) {
    return this.http.get(routes.get('getProductById') + id);
  }

  getAllTypes() {
    return this.http.get(routes.get('getAllTypes') + ''); //Fixme убери костыль
  }

  getCategoriesByTypeId(id: string) {
    return this.http.get(routes.get('getCategoriesByTypeId') + id);
  }

  getProductsByCategoryId(id: string) {
    return this.http.get(routes.get('getProductsByCategoryId') + id);
  }

  //Part users
  getUserProfile(userId: string) {
    return this.http.get(routes.get('getUserProfile') + userId);
  }

  getAllUserOrders(userId: string) {
    return this.http.get(routes.get('getAllUserOrders') + userId);
  }

  updateUserProfile(user: User) {
    return this.http.put(routes.get('updateUserProfile')!, user);
  }

  updateUserPassword(userId: string, currentPassword: string, newPassword: string) {
    let body = {userId: userId, currentPassword: currentPassword, newPassword: newPassword};
    return this.http.post(routes.get('updateUserPassword')!, body);
  }

  //Part orders
  createOrder(order: Order) {
    return this.http.post(routes.get('createOrder') + '', order);
  }

  getOrder(idOrder: string) {
    return this.http.get(routes.get('getOrderById') + idOrder);

  }

  approveOrder(order: Order) {
    return this.http.post(routes.get('approveOrder')!, order);
  }

  payOrder(order: Order) {
    return this.http.post(routes.get('payOrder')!, order);
  }

  declineOrder(order: Order) {
    return this.http.post(routes.get('declineOrder')!, order);
  }
}
