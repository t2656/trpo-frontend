import {Injectable} from '@angular/core';
import {Subject} from "rxjs";
import {CartProductItem} from "../../models/CartProductItem";
import {HttpService} from "../http/http.service";
import {Price} from "../../models/Price";
import {ShoppingCartInfo} from "../../models/ShoppingCartInfo";

const KEY_CART: string = 'cart';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCardService {

  shoppingCard: Map<string, CartProductItem>;
  eventUpdate: Subject<any>;

  constructor(private httpService: HttpService) {
    this.shoppingCard = new Map<string, CartProductItem>([]);
    this.eventUpdate = new Subject<any>();
  }

  public addProductToCard(idProduct: string, name: string, photo: string, price: Price) {
    this.updateShoppingCart();
    let product = this.shoppingCard.get(idProduct);

    if (product != undefined) {
      product.count++;
      this.shoppingCard.set(idProduct, product);
    } else {
      this.shoppingCard.set(idProduct, new CartProductItem(idProduct, name, photo, price, 1));
    }

    this.updateLocalStorage();
  }

  public deleteProductToCard(idProduct: string) {
    this.updateShoppingCart();
    let product = this.shoppingCard.get(idProduct);

    if (product != undefined) {
      if (product.count > 1) {
        product.count--;
        this.shoppingCard.set(idProduct, product);
      } else {
        this.shoppingCard.delete(idProduct);
      }

      this.updateLocalStorage();
    }
  }

  public getSizeCard(): number {
    this.updateShoppingCart();
    return this.shoppingCard.size;
  }

  public getItems(): CartProductItem[] {
    //Todo реализовать метод для формирования массива продутов для отображения в корзине
    this.updateShoppingCart();
    return Array.from(this.shoppingCard.values());
  }

  public getProductFromCart(productId: string): CartProductItem | undefined {
    this.updateShoppingCart();
    return this.shoppingCard.get(productId);
  }

  public clearCart(): void {
    this.shoppingCard = new Map<string, CartProductItem>();
    this.updateLocalStorage();
  }

  public createShoppingCartInfo(): ShoppingCartInfo {
    let countOfProducts = 0;
    let sumProducts = 0;
    let sumDiscount = 0;
    let sumFinished = 0;

    this.updateShoppingCart();
    this.shoppingCard.forEach(function (value, key, map) {
      countOfProducts += value.count;
      sumProducts += value.price.oldPrice > 0 ? value.price.oldPrice * value.count : value.price.currentPrice * value.count;
      sumDiscount += value.price.oldPrice > 0 ? (value.price.oldPrice - value.price.currentPrice) * value.count : 0;
    });

    sumFinished = sumProducts - sumDiscount;

    return new ShoppingCartInfo(countOfProducts, sumProducts, sumDiscount, sumFinished, "Р");
  }

  private updateShoppingCart() {
    let cartString = localStorage.getItem(KEY_CART);
    if (cartString != null) {
      let cart = JSON.parse(cartString);
      if (cart != null) {
        //FIXME PLEASE
        this.shoppingCard = new Map<string, CartProductItem>()
        cart.forEach((element: [string, CartProductItem]) => this.shoppingCard.set(element[0], element[1]));
      }
    } else {
      this.shoppingCard = new Map<string, CartProductItem>([]);
    }
  }

  private updateLocalStorage() {
    let jsonText = JSON.stringify(Array.from(this.shoppingCard.entries()));
    localStorage.setItem(KEY_CART, jsonText);
    this.eventUpdate.next('chained');
  }

}
