import {Injectable} from '@angular/core';
import {User} from "../../models/User";
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  user?: User;
  eventUpdateAuth: Subject<any>;
  testUser: User;

  constructor() {
    this.user = undefined;
    this.testUser = {
      id: "1",
      firstName: 'Anton',
      lastName: 'Lepeshkin',
      email: 'test_anoton@mail.ru',
      photo: 'https://pbs.twimg.com/media/EqrsnGIWMAMRN5z.jpg',
      phone: '89273334455'
    };//Todo реализовать в задаче с авторизацией
    this.eventUpdateAuth = new Subject<any>();
  }

  public signIn() {
    //Todo mock
    this.user = this.testUser;
    localStorage.setItem('user', JSON.stringify(this.user));
    this.eventUpdateAuth.next('chained');
  }

  public logout() {
    localStorage.removeItem('user');
    this.user = undefined;
    window.location.href = '/';
    this.eventUpdateAuth.next('chained');
  }

  public getUser(): User | undefined {
    let itemUser = localStorage.getItem('user');
    if (itemUser != null) {
      this.user = JSON.parse(itemUser);
    }
    return this.user;
  }
}
