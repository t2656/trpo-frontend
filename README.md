# TrpoFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.


# GiveMeFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.


NOTE:
https://medium.com/@rajaramtt/angular-style-guide-best-practices-e7ec08ad3226
https://medium.com/@motcowley/angular-folder-structure-d1809be95542
https://itnext.io/choosing-a-highly-scalable-folder-structure-in-angular-d987de65ec7


Structure of project:
modules:
home ->pages(home*, catalog, ad, createAd, profileUser) after move profile in some module
auth ->pages(login, registration, remember, password)
welcome ->pages(welcome*, about, help)
lk_user ->pages(lk-user, settings)
lk_admin ->pages(lk-admin, settings_admin_account)
errors -> pages(404, 403)

ng generate component /modules/main/components/ProfileCard --style=less
ng generate service /shared/services/HttpAdService
ng generate module Shared

lazy loading modules

mockserver -p 8080 -m integration-tests/mock-server/mocks
